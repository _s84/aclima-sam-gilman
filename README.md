# Tripplets

  Simple utility to get/print tripplets.

### Setup

    git clone https://_s84@bitbucket.org/_s84/aclima-sam-gilman.git

### Testing

    python test.py

### Usage

    import tripplets

    tripplets.print_tripplets('Hello, hello world, world hello.')
    -> hello world: 2

    tripplets.get_tripplets('Hello, hello world, world hello.')
    -> ['hello world: 2']