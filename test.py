#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
import json
import tripplets

with open('test.json') as test_file:    
    tests = json.load(test_file)


class TestTripplets(unittest.TestCase):

    def test_default(self):
        self.assertEqual(tripplets.get_tripplets(str(tests[0][0])), tests[0][1])

    def test_semicolon(self):
        self.assertEqual(tripplets.get_tripplets(str(tests[1][0])), tests[1][1])

    def test_double_spaces(self):
        self.assertEqual(tripplets.get_tripplets(str(tests[2][0])), tests[2][1])


if __name__ == '__main__':
    unittest.main()
