#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
from collections import Counter


def print_tripplets(text):
    """Print tripplets to standard output
    Args:
        text: string to get tripplets from
    Returns:
        None
    """

    tripplets = get_tripplets(text)
    for tripplet in tripplets:
        print tripplet


def get_tripplets(text):
    """Gets tripplets from text
    Args:
        text: string to get tripplets from
    Returns:
        Array 
    Raises:
        TypeError: if text is not a string.
    """

    sentences = _get_sentences(text)
    pairs_array = _get_pairs_array_from_sentences(sentences)
    pairs_dict = _get_pairs_dict(pairs_array)
    winners = _get_winners(pairs_dict)
    return winners



# There is no reason to use these functions directly; however,
# if something comes up and they need to be used let's add 
# documentation and tests.


def _get_sentences(text):
    if type(text) is not str:
        raise TypeError('text must be a string')
    sentences = re.split('[,.?;]', text.lower())
    return map(str.lstrip, sentences)


def _get_pairs_array_from_sentences(sentences):
    return reduce(_get_pairs_from_sentence, sentences, [])


def _get_pairs_from_sentence(array, sentence):
    running = []
    words = sentence.split()
    for word in words:
        running.append(word)
        if len(running) > 1:
            array.append(" ".join(frozenset(running)))
            running = [running[1]]
    return array


def _get_pairs_dict(pairs_array):
    return Counter(pairs_array)


def _get_winners(pairs_dict):
    winners = []
    for key in pairs_dict:
        count = pairs_dict[key]
        if count > 1:
            winners.append(': '.join([key, str(count)]))
    return winners
